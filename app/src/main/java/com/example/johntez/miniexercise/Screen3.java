package com.example.johntez.miniexercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Screen3 extends AppCompatActivity {

    //declaring a button

    private Button backscreen3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen3);

        //register the buttons

        backscreen3= (Button) findViewById(R.id.backbutton3);

        backscreen3.setOnClickListener(new View.OnClickListener() {

        // event

            @Override
            public void onClick(View view1) {

                //destroy current activity and goes back to main activity

                finish();

            }
        });


    }

}
