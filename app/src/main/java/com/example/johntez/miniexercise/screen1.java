package com.example.johntez.miniexercise;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class screen1 extends AppCompatActivity {

    //declaring a button object

    Button backscreen1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen1);
        //register the buttons

        backscreen1= (Button) findViewById(R.id.backbutton1);

        backscreen1.setOnClickListener(new View.OnClickListener(){

            //event

            @Override
            public void onClick(View view1) {

                //destroy current activity and goes back to main activity

                finish();

            }
        });

    }

}