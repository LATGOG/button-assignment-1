package com.example.johntez.miniexercise;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //declaring the buttons

    private Button insect;
    private Button animal;
    private Button children;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //register the buttons

        insect = (Button) findViewById(R.id.insect);
        animal = (Button) findViewById(R.id.animal);
        children = (Button) findViewById(R.id.children);

        //registering the listeners

        insect.setOnClickListener(this);

        animal.setOnClickListener(this);

        children.setOnClickListener(this);

    }
        //events following the listeners being stimulated
        @Override
        public void onClick (View views){
          if (views==insect){
                    Intent screen1 = new Intent(this, screen1.class);
                    startActivity(screen1);

            }else if(views==animal){

              Intent screen1 = new Intent(this, Screen2.class);
              startActivity(screen1);

          }else if (views==children){

              Intent screen1 = new Intent(this, Screen3.class);
              startActivity(screen1);
          }


        }
    }
