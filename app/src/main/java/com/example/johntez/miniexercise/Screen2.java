package com.example.johntez.miniexercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Screen2 extends AppCompatActivity {

    //declaring an object button

    Button backscreen2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        //register the buttons

        backscreen2= (Button) findViewById(R.id.backbutton2);

        backscreen2.setOnClickListener(new View.OnClickListener() {

                //event

                @Override
                public void onClick(View view1) {

                    //destroy current activity and goes back to main activity

                    finish();

                }
            });


    }

}